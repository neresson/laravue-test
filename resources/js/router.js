import vueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(vueRouter);

import Index from './views/Index';
import Blog from './views/Blog';
import Post from './views/Post';
import CreatePost from './views/CreatePost';
import Contacts from './views/Contacts';
import Register from './views/Register'

const routes = [
    {
        path: "/",
        name: 'main',
        component: Index
    },
    {
        path: "/blog",
        name: 'blog',
        component: Blog
    },
    {
        path: "/post/:id",
        name: "post",
        component: Post
    },
    {
        path: "/create",
        name: 'createPost',
        component: CreatePost
    },
    {
        path: "/contacts",
        name: 'contacts',
        component: Contacts
    },
    {
        name: "register",
        path: "/register",
        component: Register
    }
];



export default new vueRouter({
    mode: "history",
    routes
});
